package StepDefinitions;

import java.io.IOException;

import org.testng.Assert;

import Common_Method_Package.Trigger_API_Method;
import Request_Repository.End_Points;
import Request_Repository.Post_Request_Repository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class Post_API_StepDefinitions {
	String req_body;
	String Post_Endpoint;
	int res_Statuscode;
	String res_body;

	@Given("Enter name and job in requestbody")
	public void enter_name_and_job_in_requestbody() throws IOException {
		req_body = Post_Request_Repository.Post_TC1_Request();
		Post_Endpoint = End_Points.post_endpoint();
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the post request with request body")
	public void send_the_post_request_with_request_body() {
		res_Statuscode = Trigger_API_Method.extract_status_code(req_body, Post_Endpoint);
		res_body = Trigger_API_Method.extract_Response_Body(req_body, Post_Endpoint);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Success response is received {int} as status code")
	public void success_response_is_received_as_status_code(Integer int1) {
		Assert.assertEquals(res_Statuscode, 201);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Response body as per api guide document")
	public void response_body_as_per_api_guide_document() {
		JsonPath jsp_req = new JsonPath(req_body);

		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		JsonPath jsp = new JsonPath(res_body);

		String res_name = jsp.getString("name");
		String res_job = jsp.getString("job");
		int res_id = jsp.getInt("id");
		System.out.println("Responsebody id " + res_id);
		String res_createdAt = jsp.getString("createdAt").substring(0, 11);
		System.out.println(res_createdAt);

		// Validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);

		// throw new io.cucumber.java.PendingException();
	}

}
