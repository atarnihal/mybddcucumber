package Common_Utility_Package;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_data_reader {

	public static ArrayList<String> Read_Excel_Data(String File_name, String Sheet_name, String Test_Case_name)
			throws IOException {
		ArrayList<String> ArrayData = new ArrayList<String>();

		// Step 1:- Locate the file
		String project_Dir = System.getProperty("user.dir");
		FileInputStream fis = new FileInputStream(project_Dir + "\\Input_data\\" + File_name);

		// Step 2:- Access the located excel file
		XSSFWorkbook wb = new XSSFWorkbook(fis);

		// Step 3:- Count the number of sheets available in excel file
		int countofsheet = wb.getNumberOfSheets();
		System.out.println(countofsheet);

		// Step 4:- Access the desired sheet
		for (int i = 0; i < countofsheet; i++) {
			String sheetname = wb.getSheetName(i);
			if (sheetname.equals(Sheet_name)) {
				System.out.println("inside the sheet :" + sheetname);
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> Rows = sheet.iterator();
				while (Rows.hasNext()) {
					Row currentRow = Rows.next();

					// Step 5:- Access the row corresponding desired test Case

					if (currentRow.getCell(0).getStringCellValue().equals(Test_Case_name)) {
						Iterator<Cell> Cell = currentRow.iterator();
						while (Cell.hasNext()) {
							String Data = Cell.next().getStringCellValue();
							System.out.println(Data);
							ArrayData.add(Data);
						}
					}

				}
			}

		}
		wb.close();
		return ArrayData;

	}

}
