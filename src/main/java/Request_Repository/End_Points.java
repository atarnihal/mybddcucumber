package Request_Repository;

// 	Declaring URL
public class End_Points {

	static String hostname = "https://reqres.in/";

	public static String post_endpoint() {

		String URL = hostname + "api/users";
		System.out.println(URL);
		return URL;
	}

	// Declared Get API Base URL & Resource in class named Endpoints.
	public static String get_endpoint() {
		String URL = hostname + "api/users?page=2";
		System.out.println(URL);
		return URL;
	}

	// Declared Put API Base URL & Resource in class named Endpoints.
	public static String put_endpoint() {
		String URL = hostname + "api/users/2";
		System.out.println(URL);
		return URL;
	}

	// Declared Patch API Base URL & Resource in class named Endpoints.
	public static String patch_endpoint() {
		String URL = hostname + "api/users/2";
		System.out.println(URL);
		return URL;
		
		}
	//Declared Delete API Base URL & Resource in class named Endpoints
			public static String delete_Endpoint() {
				String URL = hostname + "api/users/2";
				return URL;
	}

}
